```bash
dotnet new blazorserver -o TodoListBlazorServer
cd TodoListBlazorServer

dotnet new blazorwasm -o TodoListBlazorwasm
cd TodoListBlazorwasm

dotnet new gitignore
git init
git add .
git commit -m "c1 Init"
git push --set-upstream https://gitlab.com/DokuroGitHub/TodoListBlazor.git master
```